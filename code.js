function button_clear() {
    document.getElementById('box').innerText = '0';
    document.getElementById('last_operation_history').innerText = '';
}

function clear_entry() {
    var current_number = document.getElementById('box').innerText;
    document.getElementById('box').innerText = current_number.slice(0, -1) || '0';
}

function calculate_percentage() {
    var current_number = document.getElementById('box').innerText;
    document.getElementById('box').innerText = parseFloat(current_number) / 100;
}

function plus_minus() {
    var current_number = document.getElementById('box').innerText;
    document.getElementById('box').innerText = parseFloat(current_number) * -1;
}

function square_root() {
    var current_number = document.getElementById('box').innerText;
    if (parseFloat(current_number) < 0) {
        alert('Cannot calculate square root of a negative number');
        return;
    }
    document.getElementById('box').innerText = Math.sqrt(parseFloat(current_number));
}

function division_one() {
    var current_number = document.getElementById('box').innerText;
    if (parseFloat(current_number) === 0) {
        alert('Cannot divide by zero');
        return;
    }
    document.getElementById('box').innerText = 1 / parseFloat(current_number);
}

function power_of() {
    var current_number = document.getElementById('box').innerText;
    document.getElementById('box').innerText = Math.pow(parseFloat(current_number), 2);
}

function backspace_remove() {
    var current_number = document.getElementById('box').innerText;
    document.getElementById('box').innerText = current_number.length > 1 ? current_number.slice(0, -1) : '0';
}

var operators = ['+', '-', '*', '/'];

function button_number(num) {
    var current_number = document.getElementById('box').innerText;

    if (operators.includes(num)) {
        if (operators.includes(current_number.charAt(current_number.length - 1))) {
            current_number = current_number.slice(0, -1) + num;
        } else {
            current_number += num;
        }
    } else if (num === '=') {
        try {
            current_number = eval(current_number);
            document.getElementById('last_operation_history').innerText = '=' + current_number;
        } catch (error) {
            alert('Invalid input');
            return;
        }
    } else if (num === '.') {
        if (current_number.includes('.')) {
            return;
        } else {
            current_number += num;
        }
    } else {
        current_number = current_number === '0' ? num : current_number + num;
    }

    document.getElementById('box').innerText = current_number;
}